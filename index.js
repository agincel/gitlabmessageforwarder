/*
	GitLab Link Scrubber
	This is extra, but it was going to bother me forever.

	Written by Adam Gincel.	
*/
const fs = require("fs");
const Discord = require("discord.js");
const client = new Discord.Client({ autofetch: [
	'MESSAGE_REACTION_ADD',
	'MESSAGE_REACTION_REMOVE',
] });


const privateConfig = require('./config.js');

client.on('ready', async () => {
	console.log('I am ready!');
});

client.on('message', async (message) => {
	if (message.author.username == "GitLab") {
		let parsedString = message.content.replace(/\(([^\)]+)\)/g, '');
		parsedString = parsedString.replace(/\s\s/g, " ");
		parsedString = parsedString.replace(/\[/g, "");
		parsedString = parsedString.replace(/\]/g, "");
		parsedString = parsedString.replace(/\)/g, "");
		parsedString = parsedString.replace(/\(/g, "");

		let start = message.content.indexOf("(http");
		let len = message.content.substring(start).indexOf(")");
		let link = message.content.substr(start, len + 1);

		await message.delete();
		
		// if a channelId is defined in config.js (not empty) then send it to that channel
		if (privateConfig.channelId) {
			let sendChannel = await message.guild.channels.find("id", privateConfig.channelId);
			await sendChannel.send(parsedString + "\n" + link); 					      
		} else {
			await message.channel.send(parsedString + "\n" + link); // otherwise, just send it to the same channel
		}
	}
});

console.log("Time to log in?");
client.login(privateConfig.discordToken).catch(function (reason) {
	console.log(reason);
});
